'use strict';

/**
 * @ngdoc overview
 * @name sumoLogicApp
 * @description
 * # sumoLogicApp
 *
 * Main module of the application.
 */
angular
  .module('sumoLogicApp', [
    'ngAnimate',
    'ngRoute',
    'ngMaterial'
  ])
  .config(function ($routeProvider, $mdIconProvider) {
    $mdIconProvider.fontSet('md', 'material-icons');
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .otherwise({
        redirectTo: '/'
      });
      $mdIconProvider
       .defaultIconSet('img/icons/sets/core-icons.svg', 24);
  });
