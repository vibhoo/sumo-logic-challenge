'use strict';

/**
 * @ngdoc function
 * @name sumoLogicApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the sumoLogicApp
 */
angular.module('sumoLogicApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
