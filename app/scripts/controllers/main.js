'use strict';

/**
 * @ngdoc function
 * @name sumoLogicApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the sumoLogicApp
 */
angular.module('sumoLogicApp')
  .controller('MainCtrl', function ($rootScope, $scope, $mdDialog) {
    $scope.openDialog = function($event)
		{
			$mdDialog.show({
				targetEvent: $event,
			 	clickOutsideToClose: false,
			 	templateUrl: 'dialog.html',
		 		locals: {items: JSONData,selection_data:$scope},
			 	onComplete: afterShowAnimation,
			 	controller: DialogController,
			 	bindToController:true,
			 	onComplete: function(){
			 	}
			});
		};

		function afterShowAnimation(){
			$scope.employee = [];
		};
  });
  function DialogController($scope, $mdDialog, $q, items, selection_data) {
		$scope.items       = items;
		$scope.mailchecked = true;
		$scope.employee    = null;
		$scope.closeDialog = function() {
	  		selection_data.team     = $scope.teamname;
	  		selection_data.employee = $scope.selectedItem;
	  		$mdDialog.hide();
		};
		$scope.allemployee = $scope.employee = [];
		var filteremployee = function(){
			var results      = JSONData;
			var team         = $scope.teamname;
			results          = results.filter(function(element, index, array){
				return element.team.search(team) !== -1;
			});
			clearFields();
			$scope.allemployee = $scope.employee = results ? results[0].employees : null;
			$scope.querySearch();
		};

		var clearFields = function(){
			$scope.selectedItem = $scope.searchText = '';
		};

		$scope.querySearch = function(value){
			var results = $scope.allemployee;
			if(value){
				results = results.filter(function(element, index, array){
					return element.toLowerCase().search(value) !== -1;
				});
			}
			$scope.employee = results;
		};

		$scope.$watch('teamname',function(value){
			if(value){
				filteremployee();
			}
		});
	}

  var JSONData = [
    {
      team: 'Engineering',
      employees: ['Lawana Fan', 'Larry Rainer', 'Rahul Malik', 'Leah Shumway']
    },
    {
      team: 'Executive',
      employees: ['Rohan Gupta', 'Ronda Dean', 'Robby Maharaj']
    },
    {
    team: 'Finance',
    employees: ['Caleb Brown', 'Carol Smithson', 'Carl Sorensen']
    },
    {
    team: 'Sales',
    employees: ['Ankit Jain', 'Anjali Maulingkar']
    }
  ];
